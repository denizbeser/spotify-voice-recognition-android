package com.denizbeser.dino;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

/**
 * Created by denizbeser on 22/11/15.
 */
public class IPReceiver extends AsyncTask<URL, Integer, Long> {

    private static Context mContext;
    private static CognitoSyncManager manager;
    private final String MY_BUCKET = "dinoserverbucket";
    private final String OBJECT_KEY = "ipAddress";
    public static String ipAddress;
    static TransferObserver observer;
    private static CognitoCachingCredentialsProvider provider;
    private static CognitoCachingCredentialsProvider credentialsProvider;




    @Override
    protected Long doInBackground(URL... params) {

        AmazonS3 s3 = new AmazonS3Client(credentialsProvider);
        TransferUtility transferUtility = new TransferUtility(s3, mContext);


        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "ipAddress.txt");

        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(file.exists()){
            System.out.println("file created");
        }else{
            System.out.println("file not created");
        }
        System.out.println(file.getAbsoluteFile());

        try {
            FileOutputStream stream = new FileOutputStream(file);
            stream.write("try".getBytes());
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        GetObjectRequest request = new GetObjectRequest(MY_BUCKET, OBJECT_KEY);
        ObjectMetadata data;
        data = s3.getObject(request, file);


        int length = (int) file.length();

        byte[] bytes = new byte[length];

        try {
            FileInputStream in = new FileInputStream(file);
            in.read(bytes);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String contents = new String(bytes);
        ipAddress = contents;

        System.out.println("ipAddress read: " + ipAddress);

        Message.setAddress(ipAddress);

        return null;
    }
    @Override
    protected void onPostExecute(Long result) {
        Toast.makeText(mContext, "Server: " + ipAddress,
                Toast.LENGTH_SHORT).show();
    }
    public static void setContext(Context c) { mContext = c;}


    public static void setProvider(CognitoCachingCredentialsProvider provider) {
        credentialsProvider = provider;
    }

    public static void setManager(CognitoSyncManager manager) {
        IPReceiver.manager = manager;
    }
}


