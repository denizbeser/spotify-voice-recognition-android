package com.denizbeser.dino;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.net.Socket;
import java.net.URL;

/**
 * Created by denizbeser on 18/11/15.
 */
public class Message extends AsyncTask<URL, Integer, Long> {
    private static String address = null;
    private static String command;
    private static Context mContext;
    boolean serverIssue = false;

    @Override
    protected Long doInBackground(URL... params) {
        Socket socket = null;
        DataOutputStream DOS = null;

        try {
            socket = new Socket(address, 4444);
        }
        catch (Exception e){
            serverIssue = true;
        }
        try{
            DOS = new DataOutputStream(socket.getOutputStream());
        }
        catch (Exception e){
            serverIssue = true;
        }
        try{
            DOS.writeUTF(command);
        }
        catch (Exception e){
            serverIssue = true;
        }
        try{
            socket.close();
        } catch (Exception e) {
            serverIssue = true;
        }


        return null;
    }
    @Override
    protected void onPostExecute(Long result) {
        if(serverIssue){
        Toast.makeText(mContext, "Connection to server failed",
                Toast.LENGTH_LONG).show();
            serverIssue = false;
        }
    }
    public static void setCommand(String s){
        command = s;
    }
    public static void setContext(Context c) { mContext = c;}
    public static void setAddress(String s){
        address = s;
    }
    public static String getAddress(){
        return address;
    }
}
