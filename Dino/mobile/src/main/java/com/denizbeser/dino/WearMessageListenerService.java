package com.denizbeser.dino;

/**
 * Created by denizbeser on 06/12/15.
 */

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;


public class WearMessageListenerService extends Service {

    private GoogleApiClient apiClient;
    private MessageApi.MessageListener messageListener;
    private Handler handler;

    @Override
    public void onCreate() {
        Log.d("Listener", "created");
        Toast.makeText(getBaseContext(), "Listener on create", Toast.LENGTH_SHORT).show();
        new IPReceiver().execute();
        handler = new Handler();

        // Create MessageListener that receives messages sent from a wearable
        messageListener = new MessageApi.MessageListener() {
            @Override
            public void onMessageReceived(final MessageEvent messageEvent) {
                Log.d("Wear:","message recieved");
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        String s = new String (messageEvent.getData());
                        Log.d("service: ", "message recieved");
                        Toast.makeText(getBaseContext(),
                                "Message received: " +s, Toast.LENGTH_SHORT).show();
                        Message.setCommand(s);
                        new Message().execute();
                    }
                });
            }
        };

        // Create GoogleApiClient
        apiClient = new GoogleApiClient.Builder(getApplicationContext()).addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(Bundle bundle) {
                // Register Node and Message listeners
                Wearable.MessageApi.addListener(apiClient, messageListener);
            }

            @Override
            public void onConnectionSuspended(int i) {}
        }).addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(ConnectionResult connectionResult) {
                if (connectionResult.getErrorCode() == ConnectionResult.API_UNAVAILABLE)
                    Toast.makeText(getApplicationContext(), "wear api unavailable", Toast.LENGTH_LONG).show();
            }
        }).addApi(Wearable.API).build();
        apiClient.connect();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}