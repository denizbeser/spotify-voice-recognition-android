package com.denizbeser.dino;


import android.content.Intent;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.regions.Regions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.List;


public class MainActivity extends ActionBarActivity {

    private static final int SPEECH_REQUEST_CODE = 0;
    private static final String IDENTITY_POOL_ID = "us-east-1:b50b1267-ebdf-409e-aee9-df507a3f5345";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //start backgorund service to listen wear
        Intent intent = new Intent(MainActivity.this, WearMessageListenerService.class);
        startService(intent);

        IPReceiver.setContext(MainActivity.this);

        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(), // Context
                IDENTITY_POOL_ID, // Identity Pool ID
                Regions.US_EAST_1 // Region
        );

        CognitoSyncManager client = new CognitoSyncManager(
                getApplicationContext(),
                Regions.US_EAST_1,
                credentialsProvider);

        final Button button = (Button) findViewById(R.id.listenButton);
        button.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // Perform action on click
                if (Message.getAddress() == null) {
                    Toast.makeText(getApplicationContext(), "Please wait until IP address is received...",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Message.setContext(MainActivity.this);
                    displaySpeechRecognizer();
                }
            }
        });

        IPReceiver.setProvider(credentialsProvider);
        IPReceiver.setManager(client);
        new IPReceiver().execute();
        Toast.makeText(getApplicationContext(), "Receiving IP address...",
                Toast.LENGTH_SHORT).show();

    }

    // Create an intent that can start the Speech Recognizer activity
    private void displaySpeechRecognizer() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        // Start the activity, the intent will be populated with the speech text
        startActivityForResult(intent, SPEECH_REQUEST_CODE);
    }

    // This callback is invoked when the Speech Recognizer returns.
    // This is where you process the intent and extract the speech text from the intent.
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        if (requestCode == SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> results = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);
            String spokenText = results.get(0);

            TextView text = (TextView) findViewById(R.id.listenedText);
            text.setText(spokenText);
            Message.setCommand(spokenText);
            new Message().execute();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}