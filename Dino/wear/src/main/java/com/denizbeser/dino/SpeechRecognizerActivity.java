package com.denizbeser.dino;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.wearable.activity.ConfirmationActivity;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Wearable;

import java.util.List;

public class SpeechRecognizerActivity extends Activity {

    //for voice recognition
    public static String voiceInput = "empty";
    private static final int SPEECH_REQUEST_CODE = 0;

    private final String MESSAGE1_PATH = "/message1";
    public static GoogleApiClient apiClient;
    public static String remoteNodeId;
    public static String remoteNodeName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(SpeechRecognizerActivity.this, MessageSenderService.class);
        startService(intent);
        displaySpeechRecognizer();
    }
    // Create an intent that can start the Speech Recognizer activity
    public void displaySpeechRecognizer() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        // Start the activity, the intent will be populated with the speech text
        startActivityForResult(intent, SPEECH_REQUEST_CODE);
    }

    // This callback is invoked when the Speech Recognizer returns.
    // This is where you process the intent and extract the speech text from the intent.
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        if (requestCode == SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> results = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);
            voiceInput = results.get(0);
            // Do something with spokenText
            Toast toast = Toast.makeText(getApplicationContext(), voiceInput, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM, 0, 30);
            toast.show();
            sendMessage();
        }
        super.onActivityResult(requestCode, resultCode, data);
        finish();
    }


    public void sendMessage() {
        Log.d("Wear:", "send message");
        Log.d("Wear:", "" + apiClient + " " + remoteNodeId + " " + remoteNodeName);
        Wearable.MessageApi.sendMessage(apiClient, remoteNodeId, MESSAGE1_PATH,
                SpeechRecognizerActivity.voiceInput.getBytes())
                .setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                    @Override
                    public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                        Intent intent = new Intent(getApplicationContext(), ConfirmationActivity.class);
                        if (sendMessageResult.getStatus().isSuccess()) {
                            Log.d("Wear:", "message sent");
                            intent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.SUCCESS_ANIMATION);
                            intent.putExtra(ConfirmationActivity.EXTRA_MESSAGE, "Message sent!");
                        } else {
                            Log.d("Wear:", "failed to send");
                            intent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.FAILURE_ANIMATION);
                            intent.putExtra(ConfirmationActivity.EXTRA_MESSAGE, "Message couldn't be sent");
                        }
                        //startActivity(intent);
                    }
                });
    }
}
