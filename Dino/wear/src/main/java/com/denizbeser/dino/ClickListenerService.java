package com.denizbeser.dino;


import android.app.Service;
import android.content.Intent;
import android.gesture.GestureOverlayView;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.speech.RecognizerIntent;
import android.speech.tts.Voice;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by denizbeser on 06/12/15.
 */
public class ClickListenerService extends Service {
    Button mButton;

    public static Boolean serviceRunning = false;

    @Override
    public void onCreate() {
        serviceRunning = true;
        super.onCreate();
        Toast toast = Toast.makeText(this, "Dino is on", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 30);
        toast.show();
        mButton = new Button(this);
        mButton.setText("\t \t \t \t \n \t");
        mButton.setBackgroundColor(Color.TRANSPARENT);

        mButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                Intent intent = new Intent(getBaseContext(), SpeechRecognizerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return false;
            }
        });

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        //Button is on the op right corner
        params.gravity = Gravity.TOP | Gravity.RIGHT;
        params.setTitle("Load Average");
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        wm.addView(mButton, params);
    }


    @Override
    public void onDestroy() {
        Toast toast = Toast.makeText(this, "Dino is off", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 30);
        toast.show();
        serviceRunning = false;
        super.onDestroy();
        if(mButton != null)
        {
            ((WindowManager) getSystemService(WINDOW_SERVICE)).removeView(mButton);
            mButton = null;
        }
    }

    @Override
    public IBinder
    onBind(Intent intent) {
        return null;
    }
}