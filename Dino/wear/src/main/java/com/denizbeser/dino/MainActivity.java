package com.denizbeser.dino;

import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends WearableActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setAmbientEnabled();

        final Button button = (Button) findViewById(R.id.button);

        if(ClickListenerService.serviceRunning){
            button.setText("Turn Dino off");
        }else{
            button.setText("Turn Dino on");
        }

        button.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // Perform action on click

                Intent intent = new Intent(MainActivity.this, ClickListenerService.class);
                Intent intent2 = new Intent(MainActivity.this, MessageSenderService.class);
                if(ClickListenerService.serviceRunning){
                    button.setText("Turn Dino on");
                    stopService(intent);
                    stopService(intent2);
                }else{
                    button.setText("Turn Dino off");
                    startService(intent);
                    startService(intent2);
                }

            }
        });

    }
}
