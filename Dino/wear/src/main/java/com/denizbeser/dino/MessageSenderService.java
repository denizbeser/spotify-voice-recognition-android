package com.denizbeser.dino;

import android.app.Activity;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.wearable.activity.ConfirmationActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.List;

/**
 * Created by denizbeser on 09/12/15.
 */
public class MessageSenderService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Wear:", "service on create");
        initializeClient();
    }
    @Override
    public void onDestroy(){
        Log.d("Wear:", "service on destroy");
    }
    private void initializeClient() {
        Log.d("Wear:", "initailize client");
        // Create GoogleApiClient
        SpeechRecognizerActivity.apiClient = new GoogleApiClient.Builder(getApplicationContext()).addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(Bundle bundle) {
                // If there is a connected node, get it's id that is used when sending messages
                Wearable.NodeApi.getConnectedNodes(SpeechRecognizerActivity.apiClient).setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
                    @Override
                    public void onResult(NodeApi.GetConnectedNodesResult getConnectedNodesResult) {
                        if (getConnectedNodesResult.getStatus().isSuccess() && getConnectedNodesResult.getNodes().size() > 0) {
                            SpeechRecognizerActivity.remoteNodeId = pickBestNodeId(getConnectedNodesResult.getNodes());
                        }
                    }
                });
            }

            @Override
            public void onConnectionSuspended(int i) {

            }
        }).addApi(Wearable.API).build();
        SpeechRecognizerActivity.apiClient.connect();
        Log.d("Wear:", "initailize client end");
    }

    private String pickBestNodeId(List<Node> nodes) {
        Log.d("Wear:", "pick node");
        String bestNodeId = null;
        // Find a nearby node or pick one arbitrarily
        for (Node node : nodes) {

            if (node.isNearby()) {
                SpeechRecognizerActivity.remoteNodeName = node.getDisplayName();
                return node.getId();
            }
            bestNodeId = node.getId();
            SpeechRecognizerActivity.remoteNodeName = node.getDisplayName();
        }
        return bestNodeId;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
